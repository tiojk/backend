class ChangeWorkHourColumn < ActiveRecord::Migration
  def change
    change_column :stores, :work_hour, :text

    Advertise.__elasticsearch__.client.indices.delete index: Advertise.index_name rescue nil

    Advertise.__elasticsearch__.client.indices.create \
      index: Advertise.index_name,
      body: { settings: Advertise.settings.to_hash, mappings: Advertise.mappings.to_hash }

    Advertise.import
    
  end
end
