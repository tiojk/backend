class CreateAdvertiseNeighborhoodRelation < ActiveRecord::Migration
  def change
    create_table :advertises_neighborhoods, id: false do |t|
      t.belongs_to :advertise, index: true
      t.belongs_to :neighborhood, index: true
    end
  end
end
