class AddColorToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :bgcolor, :string
  end
end
