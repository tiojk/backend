class AddDeliveryAndPayedToAdvertise < ActiveRecord::Migration
  def change
    add_column :advertises, :delivery, :boolean, :default => false
    add_column :advertises, :payed, :boolean, :default => false
  end
end
