class AddNeighborhoodImageForSiteToAdvertises < ActiveRecord::Migration
  def change
    add_column :advertises, :neighborhood_image_for_site, :string
  end
end
