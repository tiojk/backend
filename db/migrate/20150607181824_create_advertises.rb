class CreateAdvertises < ActiveRecord::Migration
  def change
    create_table :advertises do |t|
      t.string :title
      t.string :details
      t.string :plan
      t.datetime :begin_at
      t.datetime :end_at
      t.boolean :highlight, :default => false
      t.integer :highlight_order
      t.belongs_to :store, index: true
      
      t.timestamps null: false
    end
  end
end
