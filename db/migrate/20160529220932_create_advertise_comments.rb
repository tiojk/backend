class CreateAdvertiseComments < ActiveRecord::Migration
  def change
    create_table :advertise_comments do |t|
      t.string :user_name
      t.text :text
      t.belongs_to :rate, index: true
      t.belongs_to :advertise, index: true
      t.timestamps null: false
    end
  end
end
