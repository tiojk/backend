class AddKeywordToAdvertise < ActiveRecord::Migration
  def change
    add_column :advertises, :keywords, :string
  end
end
