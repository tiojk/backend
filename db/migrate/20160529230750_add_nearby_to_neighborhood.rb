class AddNearbyToNeighborhood < ActiveRecord::Migration
  def change
    add_column :neighborhoods, :nearby, :string
  end
end
