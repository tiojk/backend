class AddIconForSiteToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :icon_for_site, :string
  end
end
