class AddImagesToAdvertises < ActiveRecord::Migration
  def change
    add_column :advertises, :image, :string
    add_column :advertises, :highlight_image, :string
    add_column :advertises, :neighborhood_image, :string
  end
end
