class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name
      t.string :phones
      t.string :address
      t.string :lat
      t.string :lng
      t.timestamps null: false
    end

    add_index :stores, :lat
    add_index :stores, :lng
  end
end
