class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :title
      t.string :text
      t.string :price
      t.string :conditions
      t.string :image
      t.belongs_to :advertise, index: true
      t.timestamps null: false
    end
  end
end
