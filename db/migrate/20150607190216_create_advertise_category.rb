class CreateAdvertiseCategory < ActiveRecord::Migration
  def change
    create_table :advertises_categories, id: false do |t|
      t.belongs_to :advertise, index: true
      t.belongs_to :category, index: true
    end
  end
end
