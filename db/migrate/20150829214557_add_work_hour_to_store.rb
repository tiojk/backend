class AddWorkHourToStore < ActiveRecord::Migration
  def change
    add_column :stores, :work_hour, :string
  end
end
