class CreateCovers < ActiveRecord::Migration
  def change
    create_table :covers do |t|
      t.belongs_to :advertise, index: true
      t.belongs_to :neighborhood, index: true
      t.integer :order
      t.timestamps null: false
    end  
  end
end
