# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170124223848) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "advertise_comments", force: :cascade do |t|
    t.string   "user_name",    limit: 255
    t.text     "text",         limit: 65535
    t.integer  "rate_id",      limit: 4
    t.integer  "advertise_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "advertise_comments", ["advertise_id"], name: "index_advertise_comments_on_advertise_id", using: :btree
  add_index "advertise_comments", ["rate_id"], name: "index_advertise_comments_on_rate_id", using: :btree

  create_table "advertises", force: :cascade do |t|
    t.string   "title",                       limit: 255
    t.string   "details",                     limit: 255
    t.string   "plan",                        limit: 255
    t.datetime "begin_at"
    t.datetime "end_at"
    t.boolean  "highlight",                               default: false
    t.integer  "highlight_order",             limit: 4
    t.integer  "store_id",                    limit: 4
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.string   "image",                       limit: 255
    t.string   "highlight_image",             limit: 255
    t.string   "neighborhood_image",          limit: 255
    t.string   "neighborhood_image_for_site", limit: 255
    t.string   "keywords",                    limit: 255
    t.boolean  "delivery",                                default: false
    t.boolean  "payed",                                   default: false
  end

  add_index "advertises", ["store_id"], name: "index_advertises_on_store_id", using: :btree

  create_table "advertises_categories", id: false, force: :cascade do |t|
    t.integer "advertise_id", limit: 4
    t.integer "category_id",  limit: 4
  end

  add_index "advertises_categories", ["advertise_id"], name: "index_advertises_categories_on_advertise_id", using: :btree
  add_index "advertises_categories", ["category_id"], name: "index_advertises_categories_on_category_id", using: :btree

  create_table "advertises_neighborhoods", id: false, force: :cascade do |t|
    t.integer "advertise_id",    limit: 4
    t.integer "neighborhood_id", limit: 4
  end

  add_index "advertises_neighborhoods", ["advertise_id"], name: "index_advertises_neighborhoods_on_advertise_id", using: :btree
  add_index "advertises_neighborhoods", ["neighborhood_id"], name: "index_advertises_neighborhoods_on_neighborhood_id", using: :btree

  create_table "average_caches", force: :cascade do |t|
    t.integer  "rater_id",      limit: 4
    t.integer  "rateable_id",   limit: 4
    t.string   "rateable_type", limit: 255
    t.float    "avg",           limit: 24,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "parent_id",     limit: 4,   default: 0, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "icon",          limit: 255
    t.string   "icon_for_site", limit: 255
    t.string   "bgcolor",       limit: 255
    t.string   "keywords",      limit: 255
  end

  add_index "categories", ["parent_id"], name: "index_categories_on_parent_id", using: :btree

  create_table "covers", force: :cascade do |t|
    t.integer  "advertise_id",    limit: 4
    t.integer  "neighborhood_id", limit: 4
    t.integer  "order",           limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "covers", ["advertise_id"], name: "index_covers_on_advertise_id", using: :btree
  add_index "covers", ["neighborhood_id"], name: "index_covers_on_neighborhood_id", using: :btree

  create_table "neighborhoods", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "state",      limit: 255
    t.string   "city",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "nearby",     limit: 255
  end

  create_table "overall_averages", force: :cascade do |t|
    t.integer  "rateable_id",   limit: 4
    t.string   "rateable_type", limit: 255
    t.float    "overall_avg",   limit: 24,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photos", force: :cascade do |t|
    t.string   "image",        limit: 255
    t.integer  "advertise_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "photos", ["advertise_id"], name: "index_photos_on_advertise_id", using: :btree

  create_table "promotions", force: :cascade do |t|
    t.string   "title",        limit: 255
    t.string   "text",         limit: 255
    t.string   "price",        limit: 255
    t.string   "conditions",   limit: 255
    t.string   "image",        limit: 255
    t.integer  "advertise_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "promotions", ["advertise_id"], name: "index_promotions_on_advertise_id", using: :btree

  create_table "rates", force: :cascade do |t|
    t.integer  "rater_id",      limit: 4
    t.integer  "rateable_id",   limit: 4
    t.string   "rateable_type", limit: 255
    t.float    "stars",         limit: 24,  null: false
    t.string   "dimension",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rates", ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type", using: :btree
  add_index "rates", ["rater_id"], name: "index_rates_on_rater_id", using: :btree

  create_table "rating_caches", force: :cascade do |t|
    t.integer  "cacheable_id",   limit: 4
    t.string   "cacheable_type", limit: 255
    t.float    "avg",            limit: 24,  null: false
    t.integer  "qty",            limit: 4,   null: false
    t.string   "dimension",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rating_caches", ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type", using: :btree

  create_table "stores", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "phones",        limit: 255
    t.string   "address",       limit: 255
    t.float    "lat",           limit: 53
    t.float    "lng",           limit: 53
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.text     "work_hour",     limit: 65535
    t.string   "address_compl", limit: 255
  end

  add_index "stores", ["lat"], name: "index_stores_on_lat", using: :btree
  add_index "stores", ["lng"], name: "index_stores_on_lng", using: :btree

end
