class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_filter :verify_authenticity_token, :if => :json_request?

  helper_method :user_signed_in?, :current_user

  def advertises_images_to_absolute_path(resource_name,resources)
    resources.each_with_index do |res,index|
      res.keys.select{|k| k.to_s.match(/image/).present?}.each do |image_field|
        if res[image_field]['url'].present?
          image_url = res[image_field]['url'].sub(/tmp\/[^\/]+/,"#{resource_name}/#{image_field}/#{res.id}")
          resources[index][image_field]['url'] = "#{request.protocol}#{request.host_with_port}#{image_url}"
        end
      end

      ['photos','promotions'].each do |nested|
        res[nested].each_with_index do |n,n_index|
          if n['image'].present?
            resources[index][nested][n_index]['image'] = "#{request.protocol}#{request.host_with_port}#{resources[index][nested][n_index]['image']}"
          end
        end
      end
    end

    resources
  end

  def categories_icons_to_absolute_path(categories)
    categories.each_with_index do |cat,index|
      cat.keys.select{|k| k.to_s.match(/icon/).present?}.each do |image_field|
        if cat[image_field].present?
          image_url = cat[image_field].sub(/tmp\/[^\/]+/,"category/#{image_field}/#{cat[:id]}")
          categories[index][image_field] = "#{request.protocol}#{request.host_with_port}#{image_url}"
        end
      end  
    end

    categories
  end

  def json_request?
    request.format.try(:json?)
  end

  def user_signed_in?
    true
  end

  def current_user
    AdminUser.find(1)
  end
end
