class RaterController < ApplicationController

  def create
    if user_signed_in?
      obj = Advertise.find(params[:id])
      obj.rate(params[:score].to_f, current_user, 'general', params[:user_name],params[:user_comment])

      obj.touch

      render :json => {satus:'OK'}
    else
      render :json => {satus:'NO_OK'}
    end
  end
end
