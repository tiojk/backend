class AdvertisesController < ApplicationController

  def search
    advertises = index

    render json: {advertises: advertises_images_to_absolute_path('advertise',advertises)}
  end

  def autocomplete_search
    render json: {
      advertises: index.map { |ad| {title: ad.title, id: ad.id, type:'advertise'} },
      categories: Category.where("name like '%#{CGI.unescape(params[:t])}%' AND parent_id > 0").map { |c| {title: c.name, id: c.id, type:'category'} },

    }
  end

  def autocomplete_neighborhood
    render json: Neighborhood.where("name like '%#{CGI.unescape(params[:n])}%'").pluck(:name)
  end

  def index
    options_search = {}

    if params[:n].present?
      options_search[:neighborhood] = CGI.unescape(params[:n])
    end
    if params[:t].present?
      options_search[:term] = CGI.unescape(params[:t])
    end
    if params[:c].present?
      options_search[:category] = CGI.unescape(params[:c])
    end
    if params[:sort_by].present?
      options_search[:sort_by] = CGI.unescape(params[:sort_by])
    end
    if params[:l].present?
      session[:latlon] = params[:l].split(',')
      options_search[:latlon] = session[:latlon]
    end
    [:maxdistance, :isdelivery, :haspromotion, :isopened, :islongdistance].each do |param_name|
      if params[param_name].present?
        options_search[param_name] = params[param_name]
      end
    end

    if params[:id].present?
      options_search[:ids] = [params[:id]]
    end

    options_search[:order] = {latlon:session[:latlon]}
      
    advertises = AdvertiseSearch.new.search(options_search)
  end

  def covers_and_categories
    unless params[:n].present?
      render json: {
          covers: [],
          categories: [],
          nearby_neighborhoods: []
        } and return
    end

    params[:n] = CGI.unescape(params[:n])

    
    neighborhood_names = params[:n].split(',')
    neighborhood = Neighborhood.where(name: neighborhood_names).first;
    output = {
      covers:[],
      categories: categories_icons_to_absolute_path(Category.present_to_neighborhood(neighborhood_names,nil)),
      nearby_neighborhoods: (neighborhood.present? && neighborhood.nearby.present?) ? neighborhood.nearby.split(',') : []
    }

    covers = {}
    Cover.joins(:neighborhood).where("#{Neighborhood.table_name}.name IN (?)", neighborhood_names).limit(3).each do |c|
      covers[c.advertise_id] = c.order.to_i
    end

    unless covers.empty?
      output[:covers] = advertises_images_to_absolute_path('advertise',AdvertiseSearch.new.search(ids:covers.keys, order:{latlon:session[:latlon]}))
      output[:covers].sort! {|a,b| covers[a[:id].to_i] <=> covers[b[:id].to_i]}
    end

    render json: output
  end

  def categories_by_adids
    render json: {categories: categories_icons_to_absolute_path(Category.present_to_neighborhood(params[:n],params[:ids].split(',')))}
  end

  def check_for_neighborhood
    if params[:l].eql?('0,0')
      params[:n] = 'Sua posicão atual'
      advertises = []
    else
      session[:latlon] = params[:l].split(',')
      params[:n] = GeoLocation.neighborhood_by_latlong(params[:l])
      advertises = AdvertiseSearch.new.search(neighborhood:params[:n], order:{latlon:session[:latlon]})
    end
    
    render json: {
      advertises_count: advertises.size,
      neighborhoods: Neighborhood.name_by_adverties,
      current_neighborhood: params[:n]
    }
  end

  def neighborhoods
    render json: {
      neighborhoods: Neighborhood.name_by_adverties
    }
  end

  def search_user_neighborhood
    render json: {msg: GeoLocation.neighborhood_by_latlong(params[:l])}
  end

  def register
  end

  def register_send_mail
    begin
      SendMail.to_comercial(params[:contact]).deliver_now
    rescue Exception => e
      ActiveRecord::Base.logger.error("\n\n>>>>>> SEND_MAIL FAIL BECAUSE: #{e.message} \n#{e.backtrace.inspect}\n\n")
    end

    flash[:notice] = 'Solicitação realizada com sucesso. Em breve entraremos em contato com você.'

    render json: {
      status:'OK'
    }
  end

  def map_direction
    render json: GeoLocation.map_direction(params.to_a.map{|e| "#{e[0]}=#{e[1]}"})
  end

end