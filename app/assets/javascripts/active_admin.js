//= require active_admin/base
//= require jquery-ui/core
//= require jquery-ui/autocomplete
//= require autocomplete-rails

function loadSelectOptions(fromSelectBox,remote_url,toSelectId){
  $.ajax({
    contentType: 'application/json; charset=utf-8',
    url: remote_url,
    data: {'byvalue':fromSelectBox[fromSelectBox.selectedIndex].value},
    dataType: 'json',
    success: function(data) {
      var listItems = '';
      var i = 0;

      while(data[i]){
        listItems += "<option value='" + data[i][1] + "'>" + data[i][0] + "</option>";
        i++;
      }

      if (toSelectId.indexOf('_name') == -1){
        listItems = ('<option value="0">-------</option>' + listItems);
      }
      else{
        $(toSelectId).show();
      }
        

      $(toSelectId).html(listItems)
    }
  });
}

function loadAdvertisesCover(neighborhoodEl){
  showLoaderInSelector('#index_table_advertises','Carregando anúncios...');
  
  $.ajax({
    contentType: 'text/html; charset=utf-8',
    url: '/admin/cover/advertises_list',
    data: {'neighborhood_id':neighborhoodEl[neighborhoodEl.selectedIndex].value},
    success: function(data) {
      $('#paginated-collection').html(data);
    }
  });
}

function saveAdvertisesCover(){
  showLoaderInSelector('#index_table_advertises','Salvando anúncios como capa...');
  var neighborhoodEl = document.getElementById('filter-neighborhood');

  var data = $('#cover-management').serialize();
  data += '&cover%5Bneighborhood_id%5D=' + neighborhoodEl[neighborhoodEl.selectedIndex].value;

  $.ajax({
    type: 'POST',
    dataType: 'json',
    url: '/admin/cover/save',
    data: data,
    success: function(data) {
      if (data.status == 'notice'){
        var loaderEl = $('#loader');
        loaderEl.addClass('flash');
        loaderEl.addClass('flash_notice');
        loaderEl.html(data.msg);
      }
    }
  });

}

function showLoaderInSelector(toSelector,msg){
  var loaderEl = $('#loader');
  if (loaderEl.length > 0){
    loaderEl.remove();
  }
  $(toSelector).before('<div id="loader" class="center"><img src="/assets/loader.gif">'+ msg +'</div>');  

  $('#loader').show(); 
}

function searchStoreGeolocation(){
  var prefixSelector = '#advertise_store_';
  var addressEl = $(prefixSelector+'address');
  var latEl = $(prefixSelector+'lat');

  if ($('form').attr('title') == 'checked'){
    return true;
  }

  if (addressEl.val().length > 0){
    showCheckStoreAdressLoader('warning');

    var formatted_address = addressEl.val().replace(/\s/g,'+').replace(',','');

    var geocodeURL = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address='+formatted_address;

    $.ajax({
      url: geocodeURL,
      dataType: "json",
      success: function(geocoding) {
        if (geocoding.status == 'OK'){
          var lat = geocoding.results[0].geometry.location.lat;
          var lng = geocoding.results[0].geometry.location.lng;

          $(prefixSelector+'lat').attr('value',lat);
          $(prefixSelector+'lng').attr('value',lng);

          showCheckStoreAdressLoader('notice');
          $('form').attr('title','checked');
          $('form').submit();
        }
        else
        {
          showCheckStoreAdressLoader('error');
        }
      }
    });
  }
  else{
    $('form').attr('title','checked');
    $('form').submit();
  } 
  return false;
}

function showCheckStoreAdressLoader(level){
  var levels = ['warning', 'error', 'notice'];
  var messages = [
    '<img src="/assets/loader.gif" /> Validando seu endereço para obtenção das coordenadas geográficas...',
    'Endereço da loja inválido. Verifique se o endereço está correto e tente novamente. Caso persista o problema, contate o responsável pelo sistema.',
    'Verificamos e o endereço da loja é válido! Concluindo cadastro do anúncio...'
  ];
  var classPrefix = 'flash_';
  var loaderEl = $('#loader');
  for (var i = 0; i < levels.length; i++) {
    if (level == levels[i]){
      loaderEl.addClass(classPrefix + level);
      loaderEl.html(messages[i]);
    }
    else{
      loaderEl.removeClass(classPrefix + levels[i]); 
    }
  };
  loaderEl.show();
}

ADDED_CLEAR_FILE = {};
function addClearFile(fieldFile){
  if (!ADDED_CLEAR_FILE[fieldFile.id]){
    funcReset = "resetFieldFile('"+fieldFile.id+"')"
    jQuery("#"+fieldFile.id).after('<input type="button" value="desfazer seleção" onclick="'+ funcReset +'" />');  
    ADDED_CLEAR_FILE[fieldFile.id] = 1;
  }
  
}

function resetFieldFile(fieldFileId){
  document.getElementById(fieldFileId).value = "";
}


$( document ).ready(function() {
  if ($('#edit_advertise').length > 0 || $('#new_advertise').length > 0){
    $.datepicker.setDefaults({minDate: 0});
  }

  if ($('form.advertise').length > 0){
    $('form').attr('onsubmit','return searchStoreGeolocation();');
    $('form .actions').before('<div id="loader" class="flash"> </div>');
  }
  


});
