ActiveAdmin.register Advertise do
  decorate_with AdvertiseDecorator

  config.batch_actions = false

  index do
    column :title
    column :begin_at
    column :end_at
    column :store do |resource|
      resource.store_name
    end
    actions
  end

  filter :title
  filter :neighborhoods
  filter :categories

  form decorate: true, :partial => "form"

  show do
    panel "Loja" do
      attributes_table_for advertise.store do
        Store.columns.collect{ |c| c.name }.each do  |s|
          if s.match('work')
            work_hour = advertise.store.work_hour
            if work_hour.present? && work_hour[0].eql?('[')
              work_hour = JSON.parse(work_hour)
              row s do
                html = []
                work_hour.each_with_index do |w,index|
                  week = I18n.t('date.abbr_day_names')[index]
                  if w['open'].present?
                    html << "#{week} #{w['open']} às #{w['close']}"
                  end
                end

                html.join(' | ')
              end
            else
              row s
            end
          else
            row s
          end
        end
      end
    end

    attributes_table do
      Advertise.columns.collect{ |c| c.name }.reject{|c| c == 'store_id'}.each do |f|
        if f.match('image').present?
          row f do
            image_tag advertise.send(f).to_s
          end
        elsif f == 'end_at'
          row f
          row('*') do
            advertise.expired_alert
          end
        else
          row f
        end
        
      end
    end

    panel "Bairros" do
      table_for advertise.neighborhoods do
        column :name
        column :city
      end
    end

    panel "Categorias" do
      table_for advertise.categories do
        column :name
        column :parent
      end
    end

    panel "Promoções" do
      table_for advertise.promotions do
        column :title
        column :text
      end
    end
    
  end

  collection_action :cities do
    render :json => NeighborhoodDecorator.availables_cities_by_state_for_select_tag(params[:byvalue])
  end

  collection_action :neighborhoods do
    render :json => NeighborhoodDecorator.availables_by_city_for_select_tag(params[:byvalue])
  end

  collection_action :categories do
    render :json => CategoryDecorator.all_by_parent_for_select_tag(params[:byvalue])
  end

  controller do
    def permitted_params
      params.permit!
    end

    def update
      format_post
      super
    end

    def create
      format_post
      super
    end

    def format_post
      store = params[:advertise][:store]
      store.permit!
      store[:work_hour] = store[:work_hour].values.to_json

      if (store[:id].blank?)
        params[:advertise][:store] = Store.create(store)
      else
        params[:advertise][:store] = Store.find(store[:id])
        store.delete :id
        params[:advertise][:store].update(store)
      end

      neighborhoods = params[:advertise][:neighborhoods]
      params[:advertise][:neighborhoods] = Neighborhood.where(id:params[:advertise][:neighborhoods][:name])

      categories = params[:advertise][:categories]
      params[:advertise][:categories] = Category.where(id:params[:advertise][:categories][:name])

      
      promotions = {}
      params[:advertise][:promotions_attributes].each do |key, p|
        if p[:title].present? && p[:text].present? && (p[:remove_promotion].blank? || p[:remove_promotion].to_i != 1)
          promotions[key] = p
        end
        

        if p[:remove_promotion].present? && p[:remove_promotion].to_i == 1
          Promotion.delete_all(id: p[:id])
          params[:advertise][:promotions_attributes].delete(key)
        else
          params[:advertise][:promotions_attributes][key].delete(:remove_promotion)
        end

      end

      params[:advertise][:promotions_attributes] = promotions

      
      if params[:advertise][:photos_attributes].present?
        params[:advertise][:photos_attributes].each do |key,p|
          if p[:remove_image].present? && p[:remove_image].to_i == 1
            Photo.delete_all(id: p[:id])
            params[:advertise][:photos_attributes].delete(key)
          end
        end
      end
    end
  end
end
