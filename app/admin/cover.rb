ActiveAdmin.register_page 'Cover' do
  menu label: 'Gestão de capas'
  sidebar 'Pesquisar Anúncios' do
    div class:'filter_form_field filter_select' do
      label 'Estado', for: 'filter-state', class: 'label'
      select id: 'filter-state', onchange:'loadSelectOptions(this, "/admin/advertises/cities","#filter-city")' do
        option('-----','')
        NeighborhoodDecorator.availables_states.each do |s|
          option(s, value:s)
        end
      end
    end

    div class:'filter_form_field filter_select' do
      label 'Cidade', for: 'filter-city', class: 'label'
      select id: 'filter-city', onchange:'loadSelectOptions(this, "/admin/advertises/neighborhoods","#filter-neighborhood")'
    end

    div class:'filter_form_field filter_select' do
      label 'Bairro', for: 'filter-neighborhood', class: 'label'
      select id: 'filter-neighborhood', onchange:'loadAdvertisesCover(this)'
    end
  end

  content title: 'Gestão de Capas' do
    div class:'paginated_collection', id:'paginated-collection' do
      render partial: 'advertises', locals: {advertises:[], show_submit_button:false}
    end
  end

  page_action :advertises_list, method: :get do
    render partial: 'advertises', locals: {
      show_submit_button:true, 
      covers:Cover.where(neighborhood_id:params[:neighborhood_id]).inject({}){|hsh,c| hsh[c.advertise_id] = (c.order || 0) ; hsh},
      advertises:AdvertiseDecorator.availables_by_neighborhood(params[:neighborhood_id]), neighborhood_id:params[:neighborhood_id]}
  end

  page_action :save, method: :post do
    neighborhood_id = params[:cover][:neighborhood_id]
    Cover.delete_all(neighborhood_id:params[:cover][:neighborhood_id])
    
    if params[:cover][:advertises][:ids].present?
      params[:cover][:advertises][:ids].each do |id|
        cover = Cover.find_or_create_by(advertise_id:id,neighborhood_id:neighborhood_id)
        cover.order = params[:cover][:advertises][:orders][id.to_s]
        cover.save!
      end
    end

    render :json => {msg:t('active_admin.cover.msgs.success'), status:'notice'}
  end
end