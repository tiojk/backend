ActiveAdmin.register AdminUser do
  permit_params :email, :password, :password_confirmation

  config.batch_actions = false

  index do
    selectable_column
    id_column
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    column "" do |resource|
      links = ''.html_safe
      links += link_to I18n.t('active_admin.view'), resource_path(resource), :class => "member_link edit_link"
      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
      if resource.id > 1
        links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, 'data-method' => :delete, 'data-confirm' => I18n.t('active_admin.delete_confirmation'), :class => "delete_link member_link"
      end
      links
    end
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  controller do
    def destroy
      if params[:id].to_i == 1
        flash[:error] = I18n.t('active_admin.access_denied.delete_master_admin')
        redirect_to admin_admin_user_path(params[:id]) and return
      end

      super
    end
  end

end
