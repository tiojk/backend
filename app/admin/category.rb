ActiveAdmin.register Category, { :sort_order => :name_asc } do
  decorate_with CategoryDecorator

  config.batch_actions = false

  index do
    column :name
    column :parent
    actions
  end

  filter :name
  filter :parent, :collection => CategoryDecorator.all_parents_for_select_tag

  form do |f|
    f.semantic_errors 
    f.inputs do
      f.input :name
      f.input :parent_id, as: :select, collection: CategoryDecorator.all_parents_for_select_tag
      f.input :icon, as: :file, hint: ('Tamanho do icone: 67x67 <br />' + image_tag(category.icon)).html_safe
      f.input :icon_for_site, as: :file, hint: ('Tamanho do icone: 200x200 <br />' + image_tag(category.icon_for_site)).html_safe
      f.input :bgcolor, hint: 'Válido somente para categorias pais'
      f.input :keywords, hint: 'Hamburgues, Suco, Sanduíche, Café'
    end
    
    f.actions         
  end

  show do
    attributes_table do
      columns_list = Category.columns.collect{ |c| c.name }
      if (category.parent_id > 0)
        columns_list = columns_list.reject! {|name| name.match('bgcolor').present?}
      end

      columns_list.each do |f|
        if f.match('icon')
          row f do
            image_tag category.send(f).to_s
          end
        elsif f.eql?('bgcolor')
          bgcolor = category.send(f)
          color = Category.default_bgcolor

          if bgcolor.present?
            color = bgcolor       
          end

          row f do
            ('<div style="background:' + color+ '; width:30px; height:30px"></div>').html_safe
          end
          
        elsif f.eql?('parent_id')
          row f do
            category.parent.present? ? category.parent.name : ''
          end
        else
          row f
        end
        
      end
    end
  end

  controller do
    def permitted_params
      params.permit!
    end

    def update
      format_parent_id
      super
    end

    def create
      format_parent_id
      super
    end

    def format_parent_id
      if params[:category][:parent_id].blank?
        params[:category][:parent_id] = 0;
      end
    end
  end
end
