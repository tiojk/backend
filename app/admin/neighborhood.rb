ActiveAdmin.register Neighborhood do
  decorate_with NeighborhoodDecorator

  config.batch_actions = false

  index do
    column :name
    column :city
    column :state
    column :created_at
    actions
  end

  filter :name
  filter :city

  form do |f|
    f.semantic_errors 
    f.inputs do
      f.input :name
      f.input :city, as: :autocomplete, url: autocomplete_city_admin_neighborhoods_path
      f.input :state, as: :select, collection: Address.states_abbr
      f.input :nearby, as: :select, collection: options_for_select(Neighborhood.pluck(:name), (neighborhood.nearby.nil? ? [] : neighborhood.nearby.split(','))), input_html: {multiple:'multiple', style:'height:200px'}
    end
    
    f.actions         
  end

  collection_action :autocomplete_city do
    render :json => NeighborhoodDecorator.availables_cities_by_text(params[:term])
  end

  controller do
    autocomplete :neighborhood, :city

    def permitted_params
      params.permit!
    end

    def update
      format_post
      super
    end

    def create
      format_post
      super
    end

    def format_post
      nearbys = params[:neighborhood][:nearby]
      unless nearbys[0].present?
        nearbys = nearbys[1..nearbys.size]
      end
      params[:neighborhood][:nearby] = nearbys.join(',')
    end
  end
end
