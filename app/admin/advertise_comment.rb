ActiveAdmin.register AdvertiseComment do
  decorate_with AdvertiseCommentDecorator

  config.batch_actions = false

  index do
    column :user_name
    column :text
    column :rate
    column :advertise
    column :created_at
    actions
  end

  filter :advertise_title, label: 'Anúncio', as: :string

  form decorate: true, :partial => "form"

  controller do
    def permitted_params
      params.permit!
    end
  end
end
