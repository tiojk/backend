class CategoryDecorator < Draper::Decorator
  delegate_all

  class << self
    def all_parents_for_select_tag
      Category.where(parent_id:0).collect{|s| [s.name, s.id]}
    end

    def all_by_parent_for_select_tag(parent_id)
      Category.where(parent_id:parent_id).collect{|s| [s.name, s.id]}
    end
  end
end