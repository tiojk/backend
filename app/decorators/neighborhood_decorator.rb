class NeighborhoodDecorator < Draper::Decorator
  delegate_all

  class << self
    def availables_states
      Neighborhood.pluck('DISTINCT state')
    end

    def availables_cities_by_state_for_select_tag(state)
      Neighborhood.where(state:state).pluck('DISTINCT city').collect{|c|[c,c]}
    end

    def availables_by_city_for_select_tag(city)
      Neighborhood.where(city:city).collect{|s| [s.name,s.id]}
    end

    def availables_cities_by_text(text)
      Neighborhood.where('LOWER(city) LIKE ?', "#{text}%").pluck('DISTINCT city')
    end
  end
end