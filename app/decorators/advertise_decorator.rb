class AdvertiseDecorator < Draper::Decorator
  delegate_all

  class << self
    def availables_by_neighborhood(neighborhood_id)
      decorate_collection(self.joins(:neighborhoods).where("neighborhoods.id = ?",neighborhood_id))
    end
  end

  def store_name
    store.name
  end

  def expired_alert
    if end_at.present? && end_at < Time.new
      '<span class="expired-alert">ATENÇÃO: Este Anúncio expirou!</span>'.html_safe
    end
  end

  def neighborhood_image?
    neighborhood_image.present? ? 'Sim' : 'Não'
  end

end