require 'elasticsearch/model'

class Advertise < ActiveRecord::Base
  include Elasticsearch::Model

  index_name 'advertise-v2'

  after_save {__elasticsearch__.index_document}
  after_touch {__elasticsearch__.index_document}
  after_destroy {__elasticsearch__.delete_document}

  just_define_datetime_picker :begin_at
  just_define_datetime_picker :end_at

  has_and_belongs_to_many :neighborhoods
  before_destroy { neighborhoods.clear }

  has_and_belongs_to_many :categories
  
  before_destroy { categories.clear }

  belongs_to :store
  has_many :covers
  has_many :promotions
  has_many :photos
  has_many :advertise_comments

  accepts_nested_attributes_for :promotions
  accepts_nested_attributes_for :photos

  mount_uploader :image, ImageUploader
  mount_uploader :neighborhood_image, ImageUploader
  mount_uploader :neighborhood_image_for_site, ImageUploader
  mount_uploader :highlight_image, ImageUploader

  ratyrate_rateable 'general'

  settings index: { number_of_shards: 1 } do 
    mappings do
      #= this mapping not working, but it is necessary. Always you have to change Json mapping,you have to open Rails Console and run:
      #  Advertise.__elasticsearch__.client.indices.delete index: Advertise.index_name
      #  Advertise.__elasticsearch__.client.indices.create \
      #        index: Advertise.index_name,
      #         body: { settings: Advertise.settings.to_hash, mappings: Advertise.mappings.to_hash }
      #
      #
      #  Advertise.import
      indexes :location, type: 'geo_point' 
      indexes :begin_at, type: 'date'
      indexes :end_at, type: 'date'
      indexes :title, type: 'string'
      indexes :keywords, type: 'string'
      indexes :id, type: 'integer'
      indexes :has_promotion, type: 'boolean'
    end
  end
  

  def as_indexed_json(options={})
    ## IMPORTANT: The Json ouput is modified in AdvertiseSearch class
    hsh = self.as_json(
      only: [:id, :title, :keywords,:highlight, :delivery, :payed, :image, :highlight_image, :neighborhood_image, :begin_at, :end_at, :created_at],
      include: {
        neighborhoods: { only: [:name, :city, :id] },
        categories: { only: [:name,:parent_id, :id] },
        store: {only: [:name, :phones, :work_hour]}
      }
    )

    unless hsh['begin_at'].present?
      hsh['begin_at'] = hsh['created_at']
    end

    unless hsh['end_at'].present?
      hsh['end_at'] = hsh['created_at']
    end

    cached_average = self.average 'general'
    hsh['rating'] = (cached_average ? cached_average.avg : 0).to_s
    hsh['rate_count'] = (cached_average ? cached_average.qty : 0).to_s
    hsh['title_clean'] = title.downcase.remove_special_chars
    if keywords.present?
      hsh['keywords_clean'] = keywords.downcase.remove_special_chars
    end
    hsh['store']['name_clean'] = store.name.downcase.remove_special_chars
    hsh['store']['address_clean'] = (store.address.present? ? store.address.downcase.remove_special_chars : '')
    if hsh['store']['work_hour'].present? && hsh['store']['work_hour'][0].eql?('[')
      hsh['store']['work_hour'] = ActiveSupport::JSON.decode(hsh['store']['work_hour'])
    else
      hsh['store']['work_hour'] = []
    end

    hsh['highlight_order'] = 7

    if (self.payed)
      hsh['highlight_order'] = 6      
    end

    if self.highlight_order.present?
      hsh['highlight_order'] = highlight_order
    end

    
    hsh['photos'] = photos.map { |p| p.as_indexed_json }
    hsh['promotions'] = promotions.map { |p| p.as_indexed_json }
    hsh['has_promotion'] = hsh['promotions'].any?
    hsh['comments'] = advertise_comments.order('created_at desc').select { |c| c.user_name.present? && c.text.present? }.map { |c| c.as_indexed_json }

    hsh.merge(location: {
        lat: store.lat, lon: store.lng
      })
  end

  def rate(stars, user, dimension=nil, dirichlet_method=false, user_name, user_comment)
    dimension = nil if dimension.blank?

    created_rate = rates(dimension).create! do |r|
      r.stars = stars
      r.rater = user
    end

    update_rate_average(stars, dimension)

    if user_name.present? && user_comment.present?
      AdvertiseComment.create!(user_name: user_name, text: user_comment, rate_id: created_rate.id, advertise_id: self.id)
    end
  end

  def store_address
    if store.address.blank?
      'Endereço não informado'
    else
      ad_cp = store.address.split(',')
      complement = ''
      unless store.address_compl.blank?
        complement = " - #{store.address_compl}"
      end
      ad_cp[1] = ("#{ad_cp[1]}#{complement}")

      ad_cp.join(',')
    end
  end
end
