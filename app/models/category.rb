class Category < ActiveRecord::Base
  has_and_belongs_to_many :advertises
  before_destroy { advertises.clear }
  
  has_many :children, class_name: 'Category', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Category'

  mount_uploader :icon, ImageUploader
  mount_uploader :icon_for_site, ImageUploader

  default_scope { order('name asc') } 

  class << self
    def default_bgcolor
      '#CCCCCC'
    end
    def present_to_neighborhood(neighborhood_name,advertisesIds)
      categories = {}

      ids = advertisesIds
      unless ids.present?
        ids = Advertise.joins(:neighborhoods).where("neighborhoods.name IN(?) AND advertises.begin_at <= '#{Time.now}' AND advertises.end_at >= '#{Time.now}'", neighborhood_name).pluck(:id)
      end
      
      Category.select('DISTINCT(categories.id), categories.parent_id, categories.name').joins(:advertises).where(advertises:{id:ids}).where('parent_id > 0').order('name asc').each do |c|
        if categories[c.parent_id].blank?
          categories[c.parent_id] = {
            name:c.parent.name,
            bgcolor: (c.parent.bgcolor.present? ? c.parent.bgcolor : self.default_bgcolor),
            id: c.parent.id,
            icon: c.parent.icon.url,
            children:[]
          }
        end
        categories[c.parent_id][:children] << c.name 
      end

      categories.values.sort! {|a,b| a[:name] <=> b[:name]}
    end
  end

  def to_json
    {
      name:name,
      children: children.map(&:name)
    }
  end
end