class Photo < ActiveRecord::Base
  belongs_to :advertise

  mount_uploader :image, ImageUploader

  def as_indexed_json
    {image: image.url}
  end
end
