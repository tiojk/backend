class Promotion < ActiveRecord::Base
  belongs_to :advertise

  mount_uploader :image, ImageUploader

  def as_indexed_json
    {
      title: title,
      image: image.url,
      price: price,
      text: text,
      conditions: conditions
    }
  end

  def remove_promotion
  end
end
