class Store < ActiveRecord::Base
  has_many :advertises, dependent: :destroy

  1.upto(7) do |week_number|
    define_method("work_hour[#{week_number}][open]") do
      ''
    end
    define_method("work_hour[#{week_number}][close]") do
      ''
    end
  end
end
