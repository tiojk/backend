class AdvertiseComment < ActiveRecord::Base
  belongs_to :advertise

  after_save {advertise.touch}
  
  def rate
    Rate.where(id: rate_id).first
  end

  def as_indexed_json
    {
      user: user_name,
      text: text,
      rating: (rate.present? ? rate.stars : 0).to_s,
      created: created_at.strftime("%d/%m/%Y")
    }
  end
end
