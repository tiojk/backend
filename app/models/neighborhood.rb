class Neighborhood < ActiveRecord::Base
  has_and_belongs_to_many :advertises
  before_destroy { advertises.clear }
  has_many :covers

  default_scope { order('name asc, city asc, state asc') }

  class << self
    def name_by_adverties
      Neighborhood.select('distinct(name)').joins(:advertises).where("advertises.begin_at <= '#{Time.now}' AND advertises.end_at >= '#{Time.now}'").map(&:name)
    end
  end
end
