class AdvertiseSearch
  attr_accessor :options

  def lat
    @lat
  end

  def lon
    @lon
  end

  def search(options={})
    @options = options

    set_latlon(options)

    if options[:ids].present?
      results = results_by_ids(options[:ids])
    else
      results = results_by_options(options)
    end

    alter_results(results)
  end

  private

  def set_latlon(options)
    @lat = nil
    @lon = nil

    if options[:latlon].present?
      @lat = options[:latlon][0].to_f
      @lon = options[:latlon][1].to_f
    end

    if options[:order].present? && options[:order][:latlon].present?
      @lat = options[:order][:latlon][0].to_f
      @lon = options[:order][:latlon][1].to_f
    end    
  end

  def results_by_ids(ids)
    Advertise.__elasticsearch__.search(
        query: { ids: {
          values: ids
        }},
        size: 30,
        sort: build_sort
      ).results
  end

  def results_by_options(options)
    query = []
    if options[:term].present?
      query << options[:term].split(' ').collect {|t| "*#{t}*"}.join(' ')
    end

    if options[:neighborhood].present?
      neighborhoods_names = options[:neighborhood].split(',')
      join_neighborhood = neighborhoods_names.map {|n| "neighborhoods.name:#{n}"}.join(' OR ')
      if neighborhoods_names.size > 1
        join_neighborhood = "(#{join_neighborhood})"
      end
      query << join_neighborhood
    end

    if options[:category].present? && options[:term].blank?
      query << "categories.name:#{options[:category]}"
    end

    if options[:haspromotion]
      query << "has_promotion:true"
    end

    search_definition = {
      sort: build_sort,
      from:0,
      size: 100,
      query: {
        bool: {
          must: [
            {
              filtered: {
                query: {
                  match_all: {}
                },
                filter: {
                  geo_distance: {
                    distance: '5km',
                    distance_type: 'arc',
                    location: {
                      lat: lat.to_f,
                      lon: lon.to_f
                    }
                  }
                }
              }
            }
          ]
        }
      }
    }

    if query.present?
      search_definition[:query][:bool][:must][0][:filtered][:filter][:geo_distance][:distance] = '30km'
      search_definition[:query][:bool][:must][0][:filtered][:query] = {
        query_string: {
          query: query.join(' AND ')
        }
      }
    end
    Advertise.__elasticsearch__.search(search_definition).results
  end

  def build_sort
    return [
      '_geo_distance' => {
         location: {
            lat: lat,
            lon: lon
         },
         unit: 'km',
         order: 'asc',
         distance_type: 'arc'
      }
    ]
  end

  def build_date_range
    {
      begin_at:{
        lte: 'now'
      },
      end_at:{
        gte: 'now'
      }
    }
  end


  def alter_results(results)
    altered = []
    results.each_with_index do |r,index|
      source = r._source
      source.delete :title_clean
      source.delete :details_clean
      source[:store].delete :name_clean
      source[:store].delete :address_clean
      source.delete :neighborhoods


      source[:categories_names] = source[:categories].map(&:name).join(', ')
      source.delete :categories      

      source[:rating] = ("%.2f" % source[:rating].to_f)
      
      source[:store][:distance] = r['sort']
      
      is_payed_advertise = (index < 4 )
      source[:store][:phones] = format_phones(source[:store][:phones])
      source[:store][:phone_summary] = phone_summary(source[:store][:phones],is_payed_advertise)
      source[:distance_as_number] = (r['sort'].present? && address_valid?(source[:store][:address]) ? r['sort'][0].to_f : 99)
      source[:isopened] = ad_openend?(source)
      
      altered << source
    end

    #byebug

    if altered.size > 0
      #'Detalhe não informado'
      in_range = Advertise.where("id IN (#{altered.collect{|a| a['id']}.join(',')}) AND begin_at <= '#{Time.now}' AND end_at >= '#{Time.now}'").inject({}) do |hsh,a| 
        hsh[a.id] = {address: a.store_address}
        if a.details.present?
          hsh[a.id][:details] = a.details 
        else
          hsh[a.id][:details] = 'Detalhe não informado'
        end
        
        hsh
      end
      
      valids_ads = []
      altered.each do |ad|
        if in_range[ad['id']].present?
          ad['details'] = in_range[ad['id']][:details]
          ad[:store]['address'] = in_range[ad['id']][:address].split(',')[0,2].join(' - ')
          ad[:store][:distance] = format_distance(ad[:store][:distance],address_valid?(ad[:store]['address']))
          valids_ads << ad
        end
      end  

      altered = valids_ads
    end

    
    hsort = {}

    altered.each do |a|
      unless hsort.has_key?(a['highlight_order'])
        hsort[a['highlight_order']] = []
      end

      hsort[a['highlight_order']] << a
    end

    hsort.keys.each {|h| hsort[h].sort! {|x,y| x['distance_as_number'] <=> y['distance_as_number'] } }

    pre_altered = []
    hsort.keys.sort.each do |h|
      pre_altered.concat( hsort[h] )
    end

    if (options[:sort_by].present?)
      sort_components = options[:sort_by].split(' ');
      pre_altered.sort! do |x,y|
        if sort_components[1].eql?('asc')
          x[sort_components[0]] <=> y[sort_components[0]]
        else
          y[sort_components[0]] <=> x[sort_components[0]]
        end
      end
    end

    altered = []
    pre_altered.each do |ad|
      should_add = (
        (!options[:maxdistance].present? || (options[:maxdistance].present? && ad[:distance_as_number].floor <= options[:maxdistance].to_f)) &&
        (!options[:islongdistance].present? || (options[:islongdistance].present? && ad[:distance_as_number] >= 10)) &&
        (!options[:isdelivery].present? || (options[:isdelivery].present? && ad[:delivery])) &&
        (!options[:isopened].present? || (options[:isopened].present? && ad_openend?(ad))) &&
        (
          (!options[:category].present? && !options[:latlon].present?) ||
          (!options[:category].present? && options[:latlon].present?) ||
          (options[:category].present? && !options[:latlon].present?) ||
          (options[:category].present? && options[:latlon].present? && ad[:categories_names].match(options[:category]).present?)
        )
      )

      if should_add
        altered << ad
      end
    end

    altered[0..30]
  end

  def address_valid?(address)
    address.present? && address.match('não informado').nil?
  end

  def format_distance(result_sort,is_valid_address)
    if result_sort.present? && is_valid_address
      distance = ("%.3f" % result_sort[0].to_f).to_f
      if distance < 1
        distance = "#{(distance * 1000).to_i} metros"
      else
        distance_components = distance.to_s.split('.')
        if distance > "#{distance_components[0]}.4".to_f
          distance = "#{distance.ceil}Km"
        else
          distance = "#{distance_components[0]}Km"
        end
      end

      return "à #{distance}"
    end
    '0'
  end

  def format_phones(phones)
    output = []
    if phones.present?
      phone_ddd = ''
      
      phones.strip.split(/\s/).each do |phone| 
        if phone.size == 2
          phone_ddd = "#{phone}"
        end

        if phone.size > 5 #exclude ddd
          if phone.match('0800').nil?
            phone = "#{phone_ddd} #{phone}"
          end
          output << {label:phone.sub(',',''),call:format_phone_to_call(phone)}
        end
      end
    end

    return output[0..2]
  end

  def format_phone_to_call(phone_number)
    ddi = '+55'
    if phone_number.match('0800').present?
      ddi = ''
    end
    return "#{ddi}#{phone_number.sub('-','').sub(/\s/,'')}"
  end

  def phone_summary(phones, is_payed_advertise)
    if phones.size > 0
      unless is_payed_advertise
       return phones.first[:label].sub(',','')
     end 

      return phones.map(&:label).join(' ou ').sub(',','')
    end

    return ''
  end

  def ad_openend?(ad)
    opened = false

    work_hours = ad[:store][:work_hour]
    week_number = (Time.now.strftime("%u").to_i - 1) #1 is Monday
    if work_hours.size > week_number
      week_work_hour = work_hours[week_number]
      open_time = week_work_hour[:open]
      close_time = week_work_hour[:close]

      if open_time.present? && close_time.present?
        open_time_cp = open_time.split(':')
        close_time_cp = close_time.split(':')

        now_hour   = Time.now.strftime("%H").to_i
        now_minute = Time.now.strftime("%M").to_i

        if open_time_cp[0].to_i > close_time_cp[0].to_i #cross 0:00
          if open_time_cp[0].to_i < now_hour
            opened = true
          end

          if open_time_cp[0].to_i == now_hour && open_time_cp[1].to_i <= now_minute
            opened = true
          end

          #when is 0:00 or higher
          open_after_midnight = (
            now_hour < close_time_cp[0].to_i &&
            (now_hour == close_time_cp[0].to_i && close_time_cp[1].to_i < now_minute)
          )
          if open_after_midnight
            opened = true
          end

        else
          if open_time_cp[0].to_i < now_hour && close_time_cp[0].to_i > now_hour
            opened = true
          end

          if open_time_cp[0].to_i == now_hour && open_time_cp[1].to_i <= now_minute
            opened = true
          end

          if close_time_cp[0].to_i == now_hour && close_time_cp[1].to_i >= now_minute
            opened = true
          end
        end

      end
    end

    opened
  end

end