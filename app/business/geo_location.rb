require 'net/http'
class GeoLocation
  class << self
    def neighborhood_by_latlong(latlng)
      uri = URI('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&latlng='+latlng)
      location = JSON.parse(Net::HTTP.get(uri))
      if location['status'].eql?('OK')
        location['results'][0]['address_components'].each do |component|
          if component['types'].include?('neighborhood') || component['types'].include?('sublocality_level_1')  || component['types'].include?('sublocality')
            return component['long_name']
          end
        end
      end
    end

    def map_direction(query)
      uri = URI('https://maps.googleapis.com/maps/api/directions/json?'+query.join('&'))
      return Net::HTTP.get(uri);
    end
  end
end