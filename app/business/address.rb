# -*- encoding : utf-8 -*-

class Address
  REGIONS = [
      {:region => 'Sudeste', :state_name => 'Espírito Santo', :state_abbr => 'ES'},
      {:region => 'Sudeste', :state_name => 'Minas Gerais', :state_abbr => 'MG'},
      {:region => 'Sudeste', :state_name => 'Rio de Janeiro', :state_abbr => 'RJ'},
      {:region => 'Sudeste', :state_name => 'São Paulo', :state_abbr => 'SP'},

      {:region => 'Sul', :state_name => 'Paraná', :state_abbr => 'PR'},
      {:region => 'Sul', :state_name => 'Rio Grande do Sul', :state_abbr => 'RS'},
      {:region => 'Sul', :state_name => 'Santa Catarina', :state_abbr => 'SC'},

      {:region => 'Nordeste', :state_name => 'Alagoas', :state_abbr => 'AL'},
      {:region => 'Nordeste', :state_name => 'Bahia', :state_abbr => 'BA'},
      {:region => 'Nordeste', :state_name => 'Ceará', :state_abbr => 'CE'},
      {:region => 'Nordeste', :state_name => 'Maranhão', :state_abbr => 'MA'},
      {:region => 'Nordeste', :state_name => 'Paraíba', :state_abbr => 'PB'},
      {:region => 'Nordeste', :state_name => 'Pernambuco', :state_abbr => 'PE'},
      {:region => 'Nordeste', :state_name => 'Piauí', :state_abbr => 'PI'},
      {:region => 'Nordeste', :state_name => 'Rio Grande do Norte', :state_abbr => 'RN'},
      {:region => 'Nordeste', :state_name => 'Sergipe', :state_abbr => 'SE'},

      {:region => 'Norte', :state_name => 'Acre', :state_abbr => 'AC'},
      {:region => 'Norte', :state_name => 'Amapá', :state_abbr => 'AP'},
      {:region => 'Norte', :state_name => 'Amazonas', :state_abbr => 'AM'},
      {:region => 'Norte', :state_name => 'Pará', :state_abbr => 'PA'},
      {:region => 'Norte', :state_name => 'Rondônia', :state_abbr => 'RO'},
      {:region => 'Norte', :state_name => 'Roraima', :state_abbr => 'RR'},
      {:region => 'Norte', :state_name => 'Tocantins', :state_abbr => 'TO'},

      {:region => 'Centro Oeste', :state_name => 'Mato Grosso', :state_abbr => 'MT'},
      {:region => 'Centro Oeste', :state_name => 'Mato Grosso do Sul', :state_abbr => 'MS'},
      {:region => 'Centro Oeste', :state_name => 'Goiás', :state_abbr => 'GO'},
      {:region => 'Centro Oeste', :state_name => 'Distrito Federal', :state_abbr => 'DF'}
    ]
  class << self
    def abbr_state(state)
      (REGIONS.select{|r| r[:state_name] == state}.first || {})[:state_abbr]
    end

    def states_abbr
      REGIONS.collect{|r| r[:state_abbr]}.sort
    end

  end
end