class SendMail < ActionMailer::Base
  default :content_type => "text/html"

  def to_comercial(contact = {})
    @user_name = contact[:name]
    @user_email = contact[:email]
    @user_message = contact[:message]

    begin
      to = 'guiaojk@gmail.com'
      if Rails.env.development?
        to = 'pierreabreup@gmail.com'
      end

      mail(:to => to, :subject => '[Guia O JK] Solicitação de anúncio', :from => 'handmobrj@gmail.com' )
    end
  end
end
