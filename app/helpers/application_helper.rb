module ApplicationHelper
  
  def show_notice
    if notice.present?
      content_tag(:div,notice,class:'alert alert-success', id:'alert-message')
    end
  end
end
