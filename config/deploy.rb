# config valid only for current version of Capistrano

lock '3.5.0'

##DÁ UM MERGE PARA O MASTER E FAZER DEPLOY E MUDAR APACHE COM O USUÁRIO EC2 USADO RVMSUDO

set :application, 'guiajk'
set :repo_url, 'git@bitbucket.org:handmob/jk-backend.git'
set :rvm_type, :system                     # Defaults to: :auto
set :rvm_ruby_version, '2.3.0'      # Defaults to: 'default'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
 

task :finish_deploy do
  on roles(:web) do
    execute "ln -sf #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    execute "ln -sf #{shared_path}/config/.env #{release_path}/.env"
    execute "ln -sf #{shared_path}/uploads #{release_path}/public/uploads"

    execute "cd #{release_path} && rvmsudo bundle install --without development test"
    execute "cd #{release_path} && RAILS_ENV=production rake db:migrate"
    execute "cd #{release_path} && RAILS_ENV=production rake assets:clean"
    execute "cd #{release_path} && RAILS_ENV=production rake assets:precompile"
    execute "touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

after "deploy:published", 'finish_deploy'
