# coding: utf-8
class String
  def remove_special_chars
    self.gsub!(/[äáàãâ]/, 'a') 
    self.gsub!(/[ëéèê]/, 'e') 
    self.gsub!(/[ïíìî]/, 'i') 
    self.gsub!(/[öóòõô]/, 'o') 
    self.gsub!(/[üúùû]/, 'u')
    self.gsub!('ç','c')
    
    return self
  end
  
  def clear_chars_for_matcher
    self.downcase.gsub(/\s/,'').remove_special_chars
  end

end