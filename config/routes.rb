Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  get '/advertises/search'  => 'advertises#search'
  get '/advertises/autocomplete_search'  => 'advertises#autocomplete_search'
  get '/advertises/covers_and_categories' => 'advertises#covers_and_categories'
  get '/advertises/check_for_neighborhood' => 'advertises#check_for_neighborhood'
  get '/advertises/neighborhoods' => 'advertises#neighborhoods'
  get '/advertises/register' => 'advertises#register', :as => :register_advertise_form
  post '/advertises/register' => 'advertises#register_send_mail', :as => :register_advertise_mail
  get '/advertises/map_direction' => 'advertises#map_direction'
  get '/advertises/categories_by_adids' => 'advertises#categories_by_adids'
  get '/advertises/autocomplete_neighborhood'
  get '/advertises/search_user_neighborhood'
end
